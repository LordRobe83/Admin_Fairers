// ** Type import
import { VerticalNavItemsType } from "src/@core/layouts/types";

const navigation = (): VerticalNavItemsType => {
  return [
    {
      title: "dashboard",
      path: "/home",
      icon: "tabler:smart-home",
    },
    {
      sectionTitle: "virtual_faires",
    },
    {
      title: "stand_list",
      path: "/second-page",
      icon: "tabler:mail",
    },
  ];
};

export default navigation;
