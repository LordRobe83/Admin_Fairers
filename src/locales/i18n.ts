import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

import it from "./it.json";
import en from "./en.json";

export const resources = {
  en: {
    translation: en,
  },
  it: {
    translation: it,
  },
};

i18n.use(initReactI18next).init({
  fallbackLng: "en",
  lng: "en",
  resources,
  interpolation: {
    escapeValue: false,
  },
});

export default i18n
